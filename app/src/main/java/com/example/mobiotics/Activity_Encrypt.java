package com.example.mobiotics;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;



/**
 * Created by Gautam Pattar 11/12/2019
 *
 */

public class Activity_Encrypt extends AppCompatActivity {


    private EditText editText;
    private TextView textView;
    private Button encrypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_encrypt);

        editText = findViewById(R.id.editText);
        textView = findViewById(R.id.textView);
        encrypt = findViewById(R.id.encrypt);

        encrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(editText.getText())) {
                    Toast.makeText(Activity_Encrypt.this, "Enter text to Encrypt", Toast.LENGTH_SHORT).show();
                    return;
                }

                hideKeyboard();
                textView.setText(doEncrypt(editText.getText().toString().trim()));


            }
        });


    }



    public String doEncrypt(String input) {

        try {

            String out = "";
            for (int i = 0; i <= input.length(); i++) {
                int j = i + 1;
                int count = 0;
                if (j >= input.length()) {
                    j = i;
                    out = out + input.charAt(i) + String.valueOf(count + 1);
                    break;
                }

                if (input.charAt(i) != input.charAt(j)) {
                    out = out + input.charAt(i) + String.valueOf(count + 1);
                } else {
                    count = 0;
                    char let = input.charAt(i);
                    for (int k = i; k < input.length(); k++) {
                        if (input.charAt(i) == input.charAt(k)) {
                            count = count + 1;
                        } else if (input.charAt(i) != input.charAt(k)) {
                            break;
                        }

                    }
                    out = out + input.charAt(i) + String.valueOf(count);
                    i = i + count - 1;
                }
            }
//        System.out.println(out);

            //System.out.println(in);
            return out;

        } catch (Exception e) {
            e.printStackTrace();

            Toast.makeText(this, "Enter correct text format", Toast.LENGTH_SHORT).show();

            return "";
        }


    }

    public void hideKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

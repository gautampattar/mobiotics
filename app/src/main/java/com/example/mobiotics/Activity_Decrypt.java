package com.example.mobiotics;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


/**
 * Created by Gautam Pattar 11/12/2019
 *
 */

public class Activity_Decrypt extends AppCompatActivity {


    private EditText editText;
    private TextView textView;
    private Button decrypt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decrypt);

        editText = findViewById(R.id.editText);
        textView = findViewById(R.id.textView);
        decrypt = findViewById(R.id.decrypt);

        decrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (TextUtils.isEmpty(editText.getText())) {
                    Toast.makeText(Activity_Decrypt.this, "Enter text to Decrypt", Toast.LENGTH_SHORT).show();
                    return;
                }

                hideKeyboard();
                textView.setText(doDecrypt(editText.getText().toString().trim()));
            }
        });


    }

    public String doDecrypt(String input) {


        try {

            String out = "";
            for (int i = 0; i < input.length(); i = i + 2) {
                char l = input.charAt(i);

                String s = String.valueOf(input.charAt(i + 1));
                int li = Integer.parseInt(s);
                int j = 0;
                while (j < li) {
                    out = out + l;
                    j++;
                }
            }
            System.out.println(out);
            return out;

        } catch (NumberFormatException n) {
            n.fillInStackTrace();
            Toast.makeText(this, "Enter correct decrypted format", Toast.LENGTH_SHORT).show();
            return "";
        }


    }

    public void hideKeyboard()
    {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}
